## awesomeBible Designprozess

Für 2021 hat die awesomeBible Startseite ein neues Design bekommen.

## Phase 1: Ideen ⭐
Ersteinmal brauchte ich natürlich Ideen für das neue Design.

Neue Gebiete auf der Startseite
- Kategorien
- Der Vers des Tages

Da ich jetzt klare Gebiete hatte, die ich unterbringen musste, gingen meine Ideen in die Richtung von abgegrenzten Sektionen.

Man könnte die Sektionen durch  [SVG-Wellen](https://getwaves.io/)  abgrenzen, aber um das ordentlich in WordPress machen zu können, braucht man ein Plugin, oder custom CSS.

Dann habe ich einen Moment in die Richtung von  [SVG Blobs](https://www.blobmaker.app/)  überlegt, weil das ja ein Designtrend ist, das gleiche wie bei den Wellen gilt auch hier. 😢

## Phase 2: Das Finale

Dann habe ich mich für simple Farblich abgegrenzte Sektionen entschieden, und so ist das Design entstanden.


![awesomeBible FullPage Screenshot](https://cdn.hashnode.com/res/hashnode/image/upload/v1609333162459/Ibnn8OOdc.png)

## Phase 3: Die Umsetzung
Ich nutze seit dem Release den Gutenberg-Editor für Wordpress.
Ich finde Contextual-Editing und die Struktur vom Block-Editor einfach gut.

Selbst zum Schreiben von Entwürfen finde ich mich mittlerweile dort am meisten Zuhause. 🏠

*Durch meine Adern fließen /-Commands...*

Das neue Homepage-Design benutzt für das Layout den Gruppen- und den Spalten-Block.

## Phase 4: Finish! 🇫🇮
Das war's. Ich bin fertig. awesomeBible ist fertig. Dieser Artikel ist fertig.