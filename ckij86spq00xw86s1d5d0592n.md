## The Journeys of Hosting

Ich habe für awesomeBible einen Hostingprovider aussuchen müssen. Ist jetzt erstmal nichts Dramatisches, aber hätte in meinem Fall auch ganz schön back-firen 🔥 können.
Mittlerweile bin ich schon seit einem Jahr beim selben Hostingprovider, und könnte zufriedener nicht sein.

* * *

Am Anfang von awesomeBible stand die Überlegung im Raum, einen Jamstack-Hoster wie z.B. Netlify zu nutzen, das habe ich dann schnell wieder verworfen. Warum, findet sich in [diesem Artikel](https://me.awesomebible.de/jamstack).

Dann hatte ich die Wahl zwischen den vielen "großen" Hosting Providern wie zum Beispiel 1&1 ionos, Strato oder dergleichen und so "kleinen" Fischen wie Netcup und Manitu.

*Fun Fact: Ich durfte mal für 💸 jemandem eine Website (inklusive über 1.000 Emails) von 1&1 zu Strato umziehen. 1&1 und Strato haben exakt dieselbe Webmail Oberfläche.
Und das ist nicht irgendwie Rainloop oder Nextcloud...*

Ich habe mich für Manitu entschieden. Wegen dem Preis. Theoretisch hatte ich 15€ Taschengeld zur Verfügung, aber ich musste ja meinen Vater überzeugen.

## Warum ich manitu so mag (a.k.a Vorteile)
Das Admininterface von manitu ist in zwei Teile aufgeteilt:
- mein Manitu
- Manitu Siteadmin

In "Mein Manitu" werden so Dinge wie Kundendaten und das Buchen neuer Webhostingpakete untergebracht.


![Mein Manitu Screenshot](https://cdn.hashnode.com/res/hashnode/image/upload/v1607626908741/gYlcXNSPo.png)

In Manitu Siteadmin wird es gleich viel aufregender.

Dort werden Dinge die ein spezifisches Webhostingpaket (bzw. Domain) betreffen geregelt.


![siteadmin.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1607627105317/x1jCxUXA9.png)

Was ich daran besonders gut finde, dass man alles, was die Website betrifft, in einem Platz hat.

Am Anfang hatte ich echt Probleme mit der Performance meiner Website. (Das waren die Zeiten, wo ich Autoptimize und Async Javascript installiert hatte)

Da hatte ich auch überlegt zu Raidboxes zu switchen, aber habe es dann doch nicht gemacht.

Mittlerweile ist die Performance bei Manitu besser als die bei Cloudflare. (Ich weiß auch nicht wie das gehen soll. 🤷)

Ich finde Manitu besonders deswegen gut, weil sie nicht ein fertiges Produkt haben, sondern die Benutzererfahrung stetig am verbessern sind.)
Alles fängt bei Cookie-Bannern an 🍪:  ["Nochmal neuer Cookie-Hinweis auf unserer Webseite"](https://www.hostblogger.de/blog/archives/7130-Nochmal-neuer-Cookie-Hinweis-auf-unserer-Webseite.html)

Erst vor kurzem wurde Postgresql ins Webhosting mit aufgenommen. 
Cronjobs, die auch bei jeglichen Hostern fehlen gibt es bei Manitu.
PHP 8 wurde innerhalb von einem Tag nach dem Release hinzugefügt.

Der Support von manitu ist wirklich grandios.
Man bekommt innerhalb von wenigen Minuten eine Antwort.
Die Gespräche sind wirkliche Gespräche und man hat wirklich das Gefühl, da ist jemand der einem helfen will.

Ich werde weiter bei manitu bleiben und es jedem empfehlen der mich danach fragt.

### Danke fürs lesen!
