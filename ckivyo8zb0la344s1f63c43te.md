## awesomeBible Redesign

Anfang Dezember habe ich das neue Design für awesomeBible ausgerollt.
Hier ein paar Gedanken dazu.

## Welche Gefühle soll das Design auslösen?
Im neuen Design habe ich angefangen Emojis zu benutzen, um Botschaften zu unterstreichen.
Ich wollte ein "gemütliches" Design, mit warmen Farben, dass gleichzeitig professionell wirkt.

## Mein erster Versuch:
![Screenshot_2020-12-10awesomeBible.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608397480401/SY3jlkzrc.png)

Das war dann auch mein finaler Versuch. Am Ende habe ich nur noch einmal die Schriftarten ausgetauscht.

Jetzt benutze ich als Schriftarten Inter, Rubik und Nunito.