## Wie ich das Release eines Open Source Projekts plane

Ich baue gerade eine [Soziale Bibel-App](https://github.com/awesomebible/my-united).

## Warum baue ich sie?
![my.awesomeBible Homepage](https://cdn.hashnode.com/res/hashnode/image/upload/v1615661628997/GLb1JuEOX.png?auto=compress)
Ich habe angefangen, sie zu bauen, weil ich die Funktionen, die ich brauchte, in bereits existierenden Apps nicht finden konnte.

Ich habe meine Arbeit daran als Open Source zur Verfügung gestellt, weil ich glaube, dass jeder in der Lage sein sollte, frei auf die Bibel zuzugreifen, und ich möchte in der Lage sein, Software zu bauen, die von der Gemeinschaft geprägt ist, die sie benutzt.
Also habe ich das Projekt Open Source gemacht.

## Was ich bei der Entwicklung gelernt habe
Ich hatte eine Vision, ich wusste, was ich bauen wollte.

Ich entwickelte einen (für meine Situation) ziemlich guten Release-Plan:
### 1. Phase: Closed-Alpha
In dieser Phase ist das Produkt einsatzbereit, und ich öffne die Registrierung über Einladungscodes für eine ausgewählte Anzahl von Personen.
Ich bitte sie häufig darum, Fehler und Verbesserungen zu melden.
Wenn das Produkt ohne größere Bugs nutzbar ist, gehe ich in die nächste Phase über.

### 2. Phase: Open-Beta: 
Eine unbegrenzte Anzahl von Personen kann sich nun registrieren und das Produkt testen.
Jetzt beginne ich, an der Benutzeroberfläche und dem Ablauf zu feilen.
Wenn das erledigt ist, gehe ich in die letzte Phase über.

### 3. Phase: Release.
Jetzt kann sich jeder registrieren und das Produkt nutzen.
Ich schreibe eine Release-Ankündigung.