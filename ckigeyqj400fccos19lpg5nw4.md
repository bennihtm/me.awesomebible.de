## To Jamstack and not to Jamstack

Als ich awesomeBible starten wollte, hatte ich das Ziel einen Blog zu bauen, ohne viel Geld für Hosting auszugeben.
Zu der Zeit habe ich auch mit SSGs (Static Site Generators) experimentiert.
Ich hatte Hugo und Jekyll ausprobiert.

## Jamstack 
Die Seite sollte schnell sein, Jamstack und statische Blogs sind hier das perfekte Beispiel.

Das Hosting sollte wenig bis gar nichts kosten, dieses Kriterium ist mit Jamstack Hostern wie zum Beispiel Netlify oder Github Pages gegeben.
Das einzige, was hier noch Geld kosten würde, wäre die Domain. *Ersteinmal*.

### Kommentare
Ein Feature was ich unbedingt brauchte, waren Kommentare.
Die Möglichkeit der Leser, direkt ihre Meinung zu sagen.

Ein kostenloser Anbieter ist hier Disqus. *Meiner Erfahrung nach, echt nicht gut.*
Aus mehreren Gründen:
- unerwünschte Werbung
-  [Datenschutzprobleme, Breaches](https://blog.disqus.com/security-alert-user-info-breach) 

Es gibt noch einige andere Anbieter, die teils richtig gut sind, aber ich wollte für Kommentare nichts ausgeben (bzw. ich hätte meinem Vater erklären müssen, wie das mit Jamstack läuft, und wofür ich einen Drittanbieter brauche)

### Kontaktformulare
Kontaktformulare sind eine ähnliche Sache.
Ein Anbieter, der richtig gut ist, ist [Formspree](https://formspree.io/).
Und es gibt ja noch die gute alte E-Mail. 

## Not to Jamstack
Ich nehm's vorweg, ich habe mich schlussendlich doch nicht für den Jamstack entschieden. Warum?

Ich habe neben der Überlegung des Jamstack-Weges auch immer noch nach Hostingprovidern gesucht, die zu der Zeit günstig genug waren. (Mein Budget war tight. 💸)

Ziemlich schnell habe ich dann  [Manitu](https://manitu.de/)  gefunden.
Als ich den Preis gesehen habe, bin ich erstmal vom Stuhl gefallen:

![manitu_pricing.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1607457169351/9Aq9PHosu.png)

Im Vergleich dazu 1&1 ionos (Auch der 50 GB Tarif):

![ionos.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1607457286712/W7k_sPxYa.png)

Lasst euch von den 2€ nicht täuschen, im Endeffekt sind das vier Euro pro Monat.

Anmerken möchte ich auch, das manitu die 50GB Webspace über alle Dienste zieht, sprich ich habe auf awesomebible.de eine Website liegen, und alle Mails und Datenbanken werden da auch noch dazu gerechnet.

Bei ionos nur 2 GB pro E-Mail-Postfach, und nur 2 GB pro Datenbank statt 50 GB.


### Wordpress
Es ist vielleicht schon aufgefallen, dass ich für [awesomebible.de](https://awesomebible.de) WordPress benutze.

Als Theme benutze ich [Blocksy](https://creativethemes.com/blocksy/), und auf der Plugin-side-of-Things ist Jetpack und WP Super Cache.

### Kommentare
Ja lol, Kommentare gibts for free.
Ich brauche keinen Drittanbieter.

* * *

Ich glaube das wars, achja für diesen Blog benutze ich [Hashnode](https://hashnode.com/@hbenjamin/joinme).

Das wars, danke fürs lesen! ❤️