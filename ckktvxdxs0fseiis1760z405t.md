## Microsoft 365: Ein Blick auf die Vorteile einer Managed-Cloud

In meinem ersten Beitrag habe ich die Nachteile von Microsoft 365 und Cloud-basierten Apps aufgeführt:

%[https://me.awesomebible.de/microsoft-365-ein-blick-auf-die-nachteile-von-der-cloud]

In diesem Beitrag möchte ich über die Vorteile von einer Managed-Cloud reden.

## Pflege
Ein offensichtlicher Vorteil ist, dass sich kein IT-Administrator mehr um die Pflege des Systems kümmern muss - dass übernimmt der Anbieter, in diesem Fall Microsoft für einen.

## Integrität
Alles ist ein Netzwerk, die verschiedenen Apps auf der Platform greifen nahtlos ineinander.
Du kannst zum Beispiel von OneDrive direkt eine Datei als Email versenden.
Oder einen Termin, den du per Mail zugesendet bekommen hast, direkt in deinen Kalender eintragen.

## Support
Da alles von einer Firma verwaltet und betreut wird, hat man auch einen zentralen Anhaltspunkt, wenn es um Probleme geht.