## Seitenübergänge

%[https://www.youtube.com/watch?v=b0oB5Nsr8MM]

Ich habe für das  [my.awesomeBible](https://me.awesomebible.de/my-one)  Dashboard einen Seitenübergang gebaut.

Das ganze Skript ist 63 Zeilen lang, benutzt [jQuery](https://jquery.com/) - Shame on me - und [GSAP](https://greensock.com/gsap/).

Ich habe mich nicht davon abhalten lassen, so Features wie radomized Farben einzubauen.


```javascript
const colors = ["#4bedc2","#1a1423","#372549","#774c60","#b75d69","#eacdc2","#aa076b","#61045f"];
const RandomColor = colors[Math.floor(Math.random() * colors.length)];
const loadingScreen = document.getElementById("ls");
loadingScreen.style.backgroundColor = RandomColor;
``` 

[Hier](https://github.com/awesomebible/my/blob/master/dashboard/index.js) das ganze Skript:

```javascript
function delay(n) {
    n = n || 2000;
    return new Promise((done) => {
        setTimeout(() => {
            done();
        }, n);
    });
}

function pageTransition() {
    var tl = gsap.timeline();
    tl.to(".loading-screen", {
        duration: 1.2,
        width: "100%",
        left: "0%",
        ease: "Expo.easeInOut",
    });

    tl.to(".loading-screen", {
        duration: 1,
        width: "100%",
        left: "100%",
        ease: "Expo.easeInOut",
        delay: 0.3,
    });
    tl.set(".loading-screen", { left: "-100%" });
}

function contentAnimation() {
    var tl = gsap.timeline();
    tl.from(".animate-this", { duration: 1, y: 30, opacity: 0, stagger: 0.4, delay: 0.2 });
}

$(function () {
    barba.init({
        sync: true,

        transitions: [
            {
                async leave(data) {
                    const done = this.async();

                    pageTransition();
                    await delay(1000);
                    done();
                },

                async enter(data) {
                    contentAnimation();
                },

                async once(data) {
                    contentAnimation();
                },
            },
        ],
    });
});
// Make Gradient Color Random
const colors = ["#4bedc2","#1a1423","#372549","#774c60","#b75d69","#eacdc2","#aa076b","#61045f"];
const RandomColor = colors[Math.floor(Math.random() * colors.length)];
const loadingScreen = document.getElementById("ls");
loadingScreen.style.backgroundColor = RandomColor;
``` 
