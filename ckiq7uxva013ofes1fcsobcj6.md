## Simplenote

Ich benutze Simplenote für Notizen.

Ich habe mich für Simplenote entschieden, weil es so simpel und einfach ist.

![Screenshot_2020-12-15 Simplenote.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608049620804/nn1wC0tAm.png)

## Die WordPress.com Integration
Ich benutze Simplenote über Onenote, Evernote oder Google Keep, unteranderem weil es eine so gute Integration in die Tools die ich nutze hat.

Eins davon ist WordPress.com, was ich über Jetpack auf awesomeBible.de nutze.

Ich kann eine Notiz in Simplenote verfassen, und die dann direkt an WordPress.com schicken, damit der Text auf meiner Website veröffentlicht wird:

### Schritt 1: Mache eine Notiz in Simplenote

![Screenshot_20201215-173114.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608050115991/zUoO1rjla.png)

### Schritt 2: Öffne das 3-Punkte-Menü und teile die Notiz

![Screenshot_20201215-173129.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608050192819/pdLF60EDD.png)

![Screenshot_20201215-173136.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608050201343/FqinOngV2.png)

Klicke auf "WordPress-Beitrag" und wähle dann die Seite aus.

### Schau dir den Post in der WordPress-App an:

![Screenshot_20201215-173202.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608050287918/-P_efOW_t.png)

![Screenshot_20201215-173218.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608050298899/4AfPdXN5F.png)

## Simplenote ist open-source
Der Sourcecode der Web- und Appversionen ist auf [Github](https://github.com/Automattic/Simplenote-United) zu finden.

## Deine Daten gehören dir
Sie liegen zwar auf den Automattic Servern, aber du hast jederzeit die Möglichkeit deine Notizen als Markdown oder JSON zu exportieren.

## Automattic

![Screenshot_2020-12-15 Automattic.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1608051317524/aJSyEcD1q.png)

Simplenote gehört zu Automattic.
Automattic ist auch die Firma hinter WordPress.com, WooCommerce, Jetpack, Akismet und seit neustem Tumblr.

Ich persönlich vertraue Automattic, dass die mit meinen Daten keinen Mist bauen. (Und wenn, dann bin ich weg.)

### Danke für's lesen! ❤️