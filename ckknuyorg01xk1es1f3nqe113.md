## YunoHost + Nextcloud: Ein Traum fürs Self-Hosting

Ich habe nach einer Lösung gesucht, um die Fotowust in meiner Familie zu lösen.
Jeder musste von überall auf die Fotos zugreifen können, und man muss sie einfach teilen können.

Nextcloud kannte ich schon, aber mir war es zu aufwändig und riskant eine manuelle Installation zu machen, und die dann über DynDNS übers Internet freizugeben.

Und da viel mir YunoHost in die Hände.

## YunoHost
 [YunoHost](https://yunohost.org/) ist ein Traum für Self-Hosting.

![Screenshot_2021-02-02 YunoHost ist ein Serverbetriebssystem, das Self-Hosting für alle ermöglicht • YunoHost.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1612259975942/bma_RK3BG.png)

Entweder hat man bereits eine eigene Domain und kann diese mit YunoHost verbinden, oder was wahrscheinlicher ist: Man braucht eine DynDNS Domain.

Dort kommt einem YunoHost zuvor.
YunoHost bietet einem eine DynDNS Subdomain auf ```ynh.fr``` auf ```nohost.me``` oder auf ```noho.st```.

YunoHost bietet einem einen großen Katalog an hochqualitativen Apps, die man mit einem Klick installieren kann.

![Screenshot_2021-02-02 YunoHost admin.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1612260473658/Bp9VxMugJ.png)

YunoHost bietet mit den meisten Apps die im Appstore sind Single-Sign-On, sprich du erstellst einen YunoHost-Benutzer und mit dem Nutzernamen und Passwort kannst du dich in allen Apps die SSO unterstützen anmelden, ohne ein weiteres Benutzerkonto zu erstellen.

Die Adminoberfläche ist aufgeräumt.
Sie bietet alle Funktionen, die man im täglichen Gebrauch braucht, aber ist nicht unübersichtlich.

![Screenshot_2021-02-02 YunoHost admin(1).png](https://cdn.hashnode.com/res/hashnode/image/upload/v1612260680727/gHDj72oa2.png)

## Nextcloud Hub
[Nextcloud](https://nextcloud.com/) ist im Core eine einfache Filesharing-Lösung.
Aber dank Nextcloud Hub, also der Groupware-Plugins - Kalender, Kontakte, Talk, Mail und Collabora oder Onlyoffice - wird Nextcloud zu einer echten Alternative zu GSuite oder Office 365.

![Screenshot_2021-02-02 Dashboard - SolidCloud.jpg](https://cdn.hashnode.com/res/hashnode/image/upload/v1612261109030/Wv7ZDcqMH.jpeg)

## OnlyOffice
Ich habe mir den Onlyoffice Server als YunoHost-App installiert, und die OnlyOffice-Erweiterung für Nextcloud.

![Screenshot_2021-02-02 lorem docx - SolidCloud.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1612261374431/ao8To9IIR.png)

OnlyOffice ist nicht so komplex wie Office 365, deshalb fehlen einige Features die man gewöhnt ist.

Für den täglichen Gebrauch von mir reicht es aber vollkommen aus.

## Ein Schlusswort
Für mich ist Nextcloud mit OnlyOffice die perfekte Lösung.
Ich synchronisiere alle Fotos von mir dorthin, und das Signal-Backup habe ich auch in der Nextcloud.